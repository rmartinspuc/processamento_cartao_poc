var indexSectionsWithContent =
{
  0: "abcdeilmnoprstvw",
  1: "deilp",
  2: "bdpw",
  3: "abdeilmprstw",
  4: "adep",
  5: "t",
  6: "cdv",
  7: "bceilmnopstv",
  8: "lp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Pages"
};

