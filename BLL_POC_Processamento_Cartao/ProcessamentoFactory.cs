﻿using BLL_POC_Processamento_Cartao.Enum;
using BLL_POC_Processamento_Cartao.Estruturas;
using BLL_POC_Processamento_Cartao.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL_POC_Processamento_Cartao
{
    /// <summary>
    /// Classe Responsável por gerenciar o processo de padrão factory
    /// </summary>
    public static class ProcessamentoFactory
    {
        /// <summary>
        /// Criador da instância do processador. Retorna uma exceção caso não haja uma classe associada a tipo de processamento informado
        /// </summary>
        /// <param name="tipoProcessamento">Enumerador do tipo de processamento a ser criado</param>
        /// <returns></returns>
        public static IProcessamento CriarProcessadorTransacao(TipoProcessamento tipoProcessamento)
        {
            switch (tipoProcessamento)
            {
                case TipoProcessamento.Credito: return new ProcessamentoCredito();
                case TipoProcessamento.Debito: return new ProcessamentoDebito();
                case TipoProcessamento.Voucher: return new ProcessamentoVoucher();

                default: throw new ArgumentOutOfRangeException();
            }
        }
    }
}
