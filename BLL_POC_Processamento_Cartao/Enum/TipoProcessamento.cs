﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL_POC_Processamento_Cartao.Enum
{
    /// <summary>
    /// Enumerador para cada tipo de processamento.
    /// </summary>
    public enum TipoProcessamento
    {
        Credito = 1,
        Debito = 2,
        Voucher = 3
    }
}
