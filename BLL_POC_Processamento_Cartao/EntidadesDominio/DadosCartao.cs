﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL_POC_Processamento_Cartao.EntidadesDominio
{
    /// <summary>
    /// DAdos do cartão informado na UI
    /// </summary>
    public class DadosCartao
    {
        public String NomeTitular { get; set; }
        public String NumeroCartao { get; set; }
        public String ValidadeCartao { get; set; }
        public int VerificadorCartao { get; set; }

    }
}
