﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL_POC_Processamento_Cartao.EntidadesDominio
{
    /// <summary>
    /// Dados da transação informados na UI
    /// </summary>
    public class DadosTransacao
    {
        public Double Valor { get; set; }
        public DadosCartao Cartao { get; set; }
    }
}
