﻿using BLL_POC_Processamento_Cartao.EntidadesDominio;
using BLL_POC_Processamento_Cartao.Enum;
using BLL_POC_Processamento_Cartao.Interfaces;
using DAL_POC_Processamento_Cartao;
using DAL_POC_Processamento_Cartao.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL_POC_Processamento_Cartao.Estruturas
{
    /// <summary>
    /// Estrutura de processamento de cartões tipo voucher (Alimentação, Refeição, Combustível, Cultura)
    /// </summary>
    public class ProcessamentoVoucher : IProcessamento
    {
        public EntidadeProcessamentoBase AutorizarTransacao(DadosTransacao dadosTransacao)
        {
            var retorno = new EntidadeProcessamentoVoucher();
            try
            {
                var listaCartoes = Mock.LoadJson().CardList.Where(c => c.CardType == 3 && c.CardNumber == dadosTransacao.Cartao.NumeroCartao);

                if (listaCartoes.Any())
                {
                    var cartao = listaCartoes.FirstOrDefault();

                    if (dadosTransacao.Valor > cartao.Balance)
                    {
                        throw new Exception("Saldo Insuficiente");
                    }

                    retorno.StatusTansacao = 1;
                    retorno.SaldoRestante = cartao.Balance.Value - dadosTransacao.Valor;
                }
            }
            catch (Exception e)
            {
                retorno.Mensagem = e.Message;
                retorno.Excecao = e.InnerException.ToString();
            }

            return retorno;
        }
    }
}
