﻿using BLL_POC_Processamento_Cartao.EntidadesDominio;
using BLL_POC_Processamento_Cartao.Enum;
using DAL_POC_Processamento_Cartao;
using DAL_POC_Processamento_Cartao.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL_POC_Processamento_Cartao.Interfaces
{
    /// <summary>
    /// Interface das estruturas de processamento
    /// </summary>
    public interface IProcessamento
    {
        /// <summary>
        /// Método responsável por autorizar e processar transação de acordo com dados da base da processadora
        /// </summary>
        /// <param name="dadosCartao">Dados do cartão vindos da Interface gráfica</param>
        /// <returns></returns>
        EntidadeProcessamentoBase AutorizarTransacao(DadosTransacao dadosCartao);
    }
}
