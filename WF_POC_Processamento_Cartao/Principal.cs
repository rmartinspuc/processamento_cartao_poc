﻿using BLL_POC_Processamento_Cartao;
using BLL_POC_Processamento_Cartao.EntidadesDominio;
using BLL_POC_Processamento_Cartao.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_POC_Processamento_Cartao
{
    public partial class Principal : Form
    {
        public TipoProcessamento TipoProcessamento { get; set; }

        public Principal()
        {
            
            InitializeComponent();
            StartTimer();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lb_dataHora.Text = DateTime.Now.ToString("dddd dd/MM/yyyy hh:mm:ss");
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void rb_credito_CheckedChanged(object sender, EventArgs e)
        {
            ExibirDadosCredito();
            this.TipoProcessamento = TipoProcessamento.Credito;
        }

        public void ExibirDadosCredito()
        {
            if (rb_credito.Checked)
            {
                tb_codSeguranca.Visible = true;
                tb_codSeguranca.Visible = true;
                lb_validadeCartao.Visible = true;
                lb_codSeguranca.Visible = true;
            }
            else
            {
                tb_codSeguranca.Visible = false;
                tb_codSeguranca.Visible = false;
                lb_validadeCartao.Visible = false;
                lb_codSeguranca.Visible = false;
            }
        }

        private void rb_voucher_CheckedChanged(object sender, EventArgs e)
        {
            ExibirDadosCredito();
            this.TipoProcessamento = TipoProcessamento.Voucher;
        }

        private void rb_debito_CheckedChanged(object sender, EventArgs e)
        {
            ExibirDadosCredito();
            this.TipoProcessamento = TipoProcessamento.Debito;
        }

        private void btn_processar_Click(object sender, EventArgs e)
        {
            var processador = ProcessamentoFactory.CriarProcessadorTransacao(this.TipoProcessamento);

            if (ValidarCampos())
            {
                var entrada = new DadosTransacao
                {
                    Valor = Convert.ToDouble(tb_valor.Text),
                    Cartao = new DadosCartao
                    {
                        NumeroCartao = tb_numCartao.Text,
                        NomeTitular = tb_nomeTitular.Text,
                        ValidadeCartao = tb_validadeCartao.Text,
                        VerificadorCartao = Convert.ToInt32(tb_codSeguranca.Text)
                    }
                };

                var retorno = processador.AutorizarTransacao(entrada);
                lb_resultado.Visible = true;
                lb_resultado.Text = retorno.StatusTansacao.ToString();

            }
            else
            {
                lb_resultado.Visible = true;
                lb_resultado.Text = "Dados inválidos. Repita a operação!";
            }
            

        }

        System.Windows.Forms.Timer t = null;
        private void StartTimer()
        {
            t = new System.Windows.Forms.Timer();
            t.Interval = 1000;
            t.Tick += new EventHandler(timer1_Tick);
            t.Enabled = true;
        }

        private bool ValidarCampos()
        {
            if(String.IsNullOrWhiteSpace(tb_nomeTitular.Text)
                || String.IsNullOrWhiteSpace(tb_numCartao.Text)
                || String.IsNullOrWhiteSpace(tb_valor.Text)
                || (String.IsNullOrWhiteSpace(tb_validadeCartao.Text) && rb_credito.Checked)
                || (String.IsNullOrWhiteSpace(tb_codSeguranca.Text) && rb_credito.Checked))
            {
                return false;
            }

            return true;
        }

    }
}
