﻿namespace WF_POC_Processamento_Cartao
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb_voucher = new System.Windows.Forms.RadioButton();
            this.rb_credito = new System.Windows.Forms.RadioButton();
            this.rb_debito = new System.Windows.Forms.RadioButton();
            this.tb_numCartao = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_valor = new System.Windows.Forms.TextBox();
            this.lb_validadeCartao = new System.Windows.Forms.Label();
            this.tb_validadeCartao = new System.Windows.Forms.TextBox();
            this.lb_codSeguranca = new System.Windows.Forms.Label();
            this.tb_codSeguranca = new System.Windows.Forms.TextBox();
            this.btn_processar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lb_dataHora = new System.Windows.Forms.Label();
            this.lb_resultado = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_nomeTitular = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_voucher);
            this.groupBox1.Controls.Add(this.rb_credito);
            this.groupBox1.Controls.Add(this.rb_debito);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(390, 73);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Modalidade do Cartão";
            // 
            // rb_voucher
            // 
            this.rb_voucher.AutoSize = true;
            this.rb_voucher.Location = new System.Drawing.Point(280, 29);
            this.rb_voucher.Name = "rb_voucher";
            this.rb_voucher.Size = new System.Drawing.Size(65, 17);
            this.rb_voucher.TabIndex = 2;
            this.rb_voucher.TabStop = true;
            this.rb_voucher.Text = "Voucher";
            this.rb_voucher.UseVisualStyleBackColor = true;
            this.rb_voucher.CheckedChanged += new System.EventHandler(this.rb_voucher_CheckedChanged);
            // 
            // rb_credito
            // 
            this.rb_credito.AutoSize = true;
            this.rb_credito.Location = new System.Drawing.Point(148, 29);
            this.rb_credito.Name = "rb_credito";
            this.rb_credito.Size = new System.Drawing.Size(58, 17);
            this.rb_credito.TabIndex = 1;
            this.rb_credito.TabStop = true;
            this.rb_credito.Text = "Crédito";
            this.rb_credito.UseVisualStyleBackColor = true;
            this.rb_credito.CheckedChanged += new System.EventHandler(this.rb_credito_CheckedChanged);
            // 
            // rb_debito
            // 
            this.rb_debito.AutoSize = true;
            this.rb_debito.Location = new System.Drawing.Point(25, 29);
            this.rb_debito.Name = "rb_debito";
            this.rb_debito.Size = new System.Drawing.Size(56, 17);
            this.rb_debito.TabIndex = 0;
            this.rb_debito.TabStop = true;
            this.rb_debito.Text = "Débito";
            this.rb_debito.UseVisualStyleBackColor = true;
            this.rb_debito.CheckedChanged += new System.EventHandler(this.rb_debito_CheckedChanged);
            // 
            // tb_numCartao
            // 
            this.tb_numCartao.Location = new System.Drawing.Point(145, 150);
            this.tb_numCartao.Name = "tb_numCartao";
            this.tb_numCartao.Size = new System.Drawing.Size(230, 20);
            this.tb_numCartao.TabIndex = 1;
            this.tb_numCartao.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Número do Cartão";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Valor da Compra";
            // 
            // tb_valor
            // 
            this.tb_valor.Location = new System.Drawing.Point(145, 119);
            this.tb_valor.Name = "tb_valor";
            this.tb_valor.Size = new System.Drawing.Size(100, 20);
            this.tb_valor.TabIndex = 3;
            // 
            // lb_validadeCartao
            // 
            this.lb_validadeCartao.AutoSize = true;
            this.lb_validadeCartao.Location = new System.Drawing.Point(199, 232);
            this.lb_validadeCartao.Name = "lb_validadeCartao";
            this.lb_validadeCartao.Size = new System.Drawing.Size(97, 13);
            this.lb_validadeCartao.TabIndex = 6;
            this.lb_validadeCartao.Text = "Validade do Cartão";
            // 
            // tb_validadeCartao
            // 
            this.tb_validadeCartao.Location = new System.Drawing.Point(319, 229);
            this.tb_validadeCartao.Name = "tb_validadeCartao";
            this.tb_validadeCartao.Size = new System.Drawing.Size(56, 20);
            this.tb_validadeCartao.TabIndex = 5;
            // 
            // lb_codSeguranca
            // 
            this.lb_codSeguranca.AutoSize = true;
            this.lb_codSeguranca.Location = new System.Drawing.Point(25, 229);
            this.lb_codSeguranca.Name = "lb_codSeguranca";
            this.lb_codSeguranca.Size = new System.Drawing.Size(110, 13);
            this.lb_codSeguranca.TabIndex = 8;
            this.lb_codSeguranca.Text = "Código de Segurança";
            // 
            // tb_codSeguranca
            // 
            this.tb_codSeguranca.Location = new System.Drawing.Point(145, 229);
            this.tb_codSeguranca.Name = "tb_codSeguranca";
            this.tb_codSeguranca.Size = new System.Drawing.Size(31, 20);
            this.tb_codSeguranca.TabIndex = 7;
            // 
            // btn_processar
            // 
            this.btn_processar.BackColor = System.Drawing.Color.LimeGreen;
            this.btn_processar.Location = new System.Drawing.Point(118, 296);
            this.btn_processar.Name = "btn_processar";
            this.btn_processar.Size = new System.Drawing.Size(217, 61);
            this.btn_processar.TabIndex = 9;
            this.btn_processar.Text = "Processar";
            this.btn_processar.UseVisualStyleBackColor = false;
            this.btn_processar.Click += new System.EventHandler(this.btn_processar_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightYellow;
            this.panel1.Controls.Add(this.lb_resultado);
            this.panel1.Location = new System.Drawing.Point(432, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(264, 334);
            this.panel1.TabIndex = 10;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lb_dataHora
            // 
            this.lb_dataHora.AutoSize = true;
            this.lb_dataHora.Location = new System.Drawing.Point(25, 391);
            this.lb_dataHora.Name = "lb_dataHora";
            this.lb_dataHora.Size = new System.Drawing.Size(35, 13);
            this.lb_dataHora.TabIndex = 12;
            this.lb_dataHora.Text = "label5";
            // 
            // lb_resultado
            // 
            this.lb_resultado.AutoSize = true;
            this.lb_resultado.Location = new System.Drawing.Point(19, 145);
            this.lb_resultado.Name = "lb_resultado";
            this.lb_resultado.Size = new System.Drawing.Size(35, 13);
            this.lb_resultado.TabIndex = 0;
            this.lb_resultado.Text = "label6";
            this.lb_resultado.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 194);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Nome do Titular";
            // 
            // tb_nomeTitular
            // 
            this.tb_nomeTitular.Location = new System.Drawing.Point(145, 191);
            this.tb_nomeTitular.Name = "tb_nomeTitular";
            this.tb_nomeTitular.Size = new System.Drawing.Size(230, 20);
            this.tb_nomeTitular.TabIndex = 13;
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 424);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_nomeTitular);
            this.Controls.Add(this.lb_dataHora);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_processar);
            this.Controls.Add(this.lb_codSeguranca);
            this.Controls.Add(this.tb_codSeguranca);
            this.Controls.Add(this.lb_validadeCartao);
            this.Controls.Add(this.tb_validadeCartao);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_valor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_numCartao);
            this.Controls.Add(this.groupBox1);
            this.Name = "Principal";
            this.Text = "Processamento de Cartão";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rb_voucher;
        private System.Windows.Forms.RadioButton rb_credito;
        private System.Windows.Forms.RadioButton rb_debito;
        private System.Windows.Forms.TextBox tb_numCartao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_valor;
        private System.Windows.Forms.Label lb_validadeCartao;
        private System.Windows.Forms.TextBox tb_validadeCartao;
        private System.Windows.Forms.Label lb_codSeguranca;
        private System.Windows.Forms.TextBox tb_codSeguranca;
        private System.Windows.Forms.Button btn_processar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lb_dataHora;
        private System.Windows.Forms.Label lb_resultado;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_nomeTitular;
    }
}

