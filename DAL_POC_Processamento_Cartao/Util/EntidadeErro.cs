﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_POC_Processamento_Cartao.Util
{
    /// <summary>
    /// 
    /// </summary>
    public class EntidadeErro
    {
        /// <summary>
        /// 
        /// </summary>
        public String Mensagem { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CodigoErro { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Excecao { get; set; }
    }
}
