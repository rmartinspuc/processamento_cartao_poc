﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_POC_Processamento_Cartao
{
    /// <summary>
    /// Classe de Mock para projeto de processadora
    /// </summary>
    public static class Mock
    {
        /// <summary>
        /// Método responsável por carregar arquivo de mock no formato json em 'Arquivos/BaseProcessadora.json'
        /// </summary>
        /// <returns></returns>
        public static  ListaCartaoProcessadora LoadJson()
        {
            ListaCartaoProcessadora retorno = new ListaCartaoProcessadora();
            using (StreamReader r = new StreamReader("Arquivos\\BaseProcessadora.json"))
            {
                string json = r.ReadToEnd();
                retorno = JsonConvert.DeserializeObject<ListaCartaoProcessadora>(json);
            }

            return retorno;
        }
    }

    /// <summary>
    /// Classe Auxiliar para utilização do arquivo json após deserialização
    /// </summary>
    public class ListaCartaoProcessadora
    {
        /// <summary>
        /// lista de cartões da base da processadora
        /// </summary>
        public List<ItemCartaoProcessadora> CardList { get; set; }
    }

    /// <summary>
    /// Classe Auxiliar para utilização do arquivo json após deserialização
    /// </summary>
    public class ItemCartaoProcessadora
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Número do cartão
        /// </summary>
        public String CardNumber { get; set; }
        /// <summary>
        /// Tipo do cartão
        /// </summary>
        public int CardType { get; set; }
        /// <summary>
        /// Proprietário do cartão
        /// </summary>
        public String Owner { get; set; }
        /// <summary>
        /// Limite do cartão - apenas na função crédito
        /// </summary>
        public Double? Limit { get; set; }
        /// <summary>
        /// fator de segurança do cartão = últimos 4 dígitos do cartão * código de segurança - apenas na função crédito
        /// </summary>
        public Int64? SecureFactor { get; set; }
        /// <summary>
        /// data de expiração do cartão = DDAAAA - apenas na função crédito
        /// </summary>
        public String ExpireDate { get; set; }
        /// <summary>
        /// Saldo do cartão - apenas na função débito e voucher
        /// </summary>
        public Double? Balance { get; set; }
        /// <summary>
        /// Valor do cheque especial - apenas na função débito
        /// </summary>
        public Double? Over { get; set; }
    }
}
