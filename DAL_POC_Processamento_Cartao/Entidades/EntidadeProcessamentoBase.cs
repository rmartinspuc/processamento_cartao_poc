﻿using DAL_POC_Processamento_Cartao.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_POC_Processamento_Cartao.Entidades
{
    /// <summary>
    /// 
    /// </summary>
    public class EntidadeProcessamentoBase : EntidadeErro
    {
        /// <summary>
        /// 
        /// </summary>
        public double ValorTransacao { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int StatusTansacao { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TipoTransacao { get; set; }

    }
}
