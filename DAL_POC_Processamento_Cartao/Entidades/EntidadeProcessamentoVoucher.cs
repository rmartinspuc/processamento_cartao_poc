﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_POC_Processamento_Cartao.Entidades
{
    /// <summary>
    /// 
    /// </summary>
    public class EntidadeProcessamentoVoucher : EntidadeProcessamentoBase
    {
        /// <summary>
        /// Saldo Restante do cartão
        /// </summary>
        public double SaldoRestante { get; set; }
    }
}
