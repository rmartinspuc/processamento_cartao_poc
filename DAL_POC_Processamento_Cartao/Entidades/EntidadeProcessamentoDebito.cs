﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_POC_Processamento_Cartao.Entidades
{
    /// <summary>
    /// 
    /// </summary>
    public class EntidadeProcessamentoDebito: EntidadeProcessamentoBase
    {
        /// <summary>
        /// Informa se o cheque especial do cliente foi utilizado
        /// </summary>
        public bool ChequeEspecial { get; set; }

    }
}
