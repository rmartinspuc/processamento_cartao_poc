﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_POC_Processamento_Cartao.Entidades
{
    /// <summary>
    /// 
    /// </summary>
    public class EntidadeProcessamentoCredito : EntidadeProcessamentoBase
    {
        /// <summary>
        /// Quantidade de pontos acumulados na transação
        /// </summary>
        public int PontosAcumulados { get; set; }

    }
}
